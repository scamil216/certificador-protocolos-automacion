
from datetime import datetime
from io import StringIO, BytesIO
from flask import Flask, request, redirect, render_template, stream_with_context
from flask_socketio import SocketIO, emit
from werkzeug.datastructures import Headers
from werkzeug.wrappers import Response


import sys, os, configparser, time, threading, csv, string, datetime
#sys.path.insert(1, "PATH TO LOCAL PYTHON PACKAGES")  #OPTIONAL: Only if need to access Python packages installed on a local (non-global) directory
#sys.path.insert(2, "PATH TO FLASK DIRECTORY")      #OPTIONAL: Only if you need to add the directory of your flask app

config = configparser.ConfigParser()
config.read('config.ini')

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, ping_timeout=720000)

check = 0
local = 0
results = []


#####################
#       Inicio      #
#####################

@app.route('/')                                                         # Pantalla Principal
def sql_database():
    global results
    results = []
    from app.sqlquery import sql_query
    protocolos = sql_query(''' SELECT * FROM Protocolos''')
    tareas = sql_query(''' SELECT * FROM Tareas''')
    return render_template('inicio.html', protocolos=protocolos, tareas=tareas)

@app.route('/Resultados')                                                         # Pantalla Principal
def sql_Rest():
    # from app.sqlquery import sql_query
    # from app.Certificar import generarMensajes, Prueba
    # from app.conexion import tcp_connect, tcp_close, tcpCli, envioMensaje
    # msg = []
    # global results
    # eventos = sql_query(' SELECT Indice FROM Eventos WHERE "ID Protocolo" = "' + proto + '"')
    # if not local: cli = threading.Thread(target=tcpCli, args=(eventos,)).start()
    # time.sleep(0.1)
    # s, conn, addr = tcp_connect()
    # for m in eventos:
    #     rel = envioMensaje(generarMensajes(int(proto),m[0],"",""), check, local, conn)
    #     results.append(rel)
    #     time.sleep(0.2)
    return render_template('Resultados.html', msg = results)

@app.route('/Resultados_log')
def descargar_log():
    def generate():
        global results
        data = StringIO()
        archivo = csv.writer(data)

        archivo.writerow(('Mensaje', 'Resultado_ID', 'Resultado_corto', 'Resultado_largo'))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)
        print(results)

        for item in results:
            archivo.writerow((
                item[0].replace("\n",""),
                item[1],
                item[2],
                item[3],
            ))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)
    headers = Headers()
    headers.set('Content-Disposition', 'attachment', filename='log ' + str(datetime.datetime.now()) +'.csv')
    return Response(stream_with_context(generate()),mimetype='text/csv', headers=headers)



#####################
#     Opciones      #
#####################


@app.route('/opciones')                                                 # Opciones del servidor
def opciones():
    data = []
    data.append(config['OPCIONES']['NEVULA_TCP_IP'])
    data.append(config['OPCIONES']['NEVULA_TCP_PORT'])
    data.append(config['OPCIONES']['LOG_SSH_IP'])
    data.append(config['OPCIONES']['LOG_SSH_PORT'])
    data.append(config['OPCIONES']['TIMEOUT_TCP'])
    data.append(config['OPCIONES']['TIMEOUT_MSG'])
    return render_template('opciones.html', status=[check, local], data=data)


#####################
# Opciones - Tareas #
#####################

@app.route('/opciones/tarea')                                           # Pagina principal de tareas
def sql_tarea():
    from app.sqlquery import sql_query
    tareas = sql_query(''' SELECT * FROM Tareas''')
    return render_template('opciones-tareas.html',tareas=tareas, selectt=["",""])

@app.route('/opciones/edit_tarea')                                      # Lista de mensajes de una tarea
def sql_tarea_query():
    from app.sqlquery import sql_query, sql_queryM
    tarea = request.args.get('tareaS')
    selectN= sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (tarea,))
    results = sql_query(' SELECT * FROM TareaDetalles INNER JOIN Protocolos on Protocolos.ID = TareaDetalles.ProtocoloID WHERE "IDTarea" = "' + tarea + '";')
    return render_template('opciones-tareas.html', select=tarea, selectt = selectN[0]  , results=results, tareaS = tarea)

@app.route('/opciones/tareas_n',methods = ['POST', 'GET'])         # Creacion de nueva tarea
def sql_tarea_tcreate():
    from app.sqlquery import sql_query, sql_edit_insert
    if request.method == 'POST':
        sql_edit_insert(''' INSERT into Tareas(Nombre,Descripcion) VALUES(?,?) ''', (request.form['name'], ""))
    tareas = sql_query(''' SELECT * FROM Tareas''')
    return render_template('opciones-tareas.html', tareas=tareas, selectt=["",""] )

@app.route('/opciones/edit_tareas_m',methods = ['POST', 'GET'])         # Creacion de un nuevo mensaje
def sql_tarea_mcreate():
    from app.sqlquery import sql_query,sql_queryM, sql_edit_insert
    tarea = request.args.get('TareaS')
    results = sql_query(' SELECT * FROM TareaDetalles INNER JOIN Protocolos on Protocolos.ID = TareaDetalles.ProtocoloID WHERE "IDTarea" = "' + tarea + '";')
    selectN = sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (tarea,))
    if (str(request.form['Pid']) == str(request.form['Evn']) == str(request.form['Cli']) == str(request.form['Zon']) == "") or str(request.form['Evn']) == "Evento":
        return render_template('opciones-tareas.html',results=results, selectt = selectN[0])
    print (sql_query('SELECT MAX(IDMensaje) AS member_id FROM TareaDetalles where IDTarea = "' + tarea + '"')[0][0])
    if request.method == 'POST':
        tareasM = sql_query('SELECT MAX(IDMensaje) AS member_id FROM TareaDetalles where IDTarea = "' + tarea + '"')[0][0]
        if tareasM is None:
            tareasM = 1
        else:
            tareasM = str( int( tareasM + 1) )
        #tareasM = str( int( sql_query('SELECT MAX(IDMensaje) AS member_id FROM TareaDetalles where IDTarea = "' + tarea + '"')[0][0] + 1) )
        sql_edit_insert(''' INSERT into TareaDetalles VALUES(?,?,?,?,?,?,?,?) ''', (tarea, tareasM, str(request.form['Pid']), str(request.form['Evn']), str(request.form['Cli']), str(request.form['Zon']), "", "") )
    results = sql_query(' SELECT * FROM TareaDetalles INNER JOIN Protocolos on Protocolos.ID = TareaDetalles.ProtocoloID WHERE "IDTarea" = "' + tarea + '";')
    selectN = sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (tarea,))
    return render_template('opciones-tareas.html',results=results, selectt = selectN[0])

@app.route('/opciones/edit_tarea_e',methods = ['POST', 'GET'])          # Editar un mensaje de una tarea
def sql_tarea_medit():
    from app.sqlquery import sql_query, sql_queryM, sql_edit_insert
    if str(request.form['PidE']) == str(request.form['EvnE']) == str(request.form['CliE']) == str(request.form['ZonE']) == "":
        return render_template('opciones-tareas.html',results=results, selectt = selectN[0])
    sql_edit_insert(''' UPDATE TareaDetalles set "ProtocoloID"=?,"Evento"=?,"Cliente"=?,"Zona"=? WHERE "IDTarea"=? AND "IDMensaje"=? ''', (str(request.form['PidE']), str(request.form['EvnE']), str(request.form['CliE']), str(request.form['ZonE']), str(request.form['oldTarea']), str(request.form['oldMens']) ) )
    results = sql_query(' SELECT * FROM TareaDetalles INNER JOIN Protocolos on Protocolos.ID = TareaDetalles.ProtocoloID WHERE "IDTarea" = "' + str(request.form['oldTarea']) + '";')
    selectN= sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (str(request.form['oldTarea']),))
    return render_template('opciones-tareas.html',results=results, selectt = selectN[0])

@app.route('/opciones/edit_tarea_d')                                    # Eliminar un mensaje de una tarea
def sql_tarea_mdelete():
    from app.sqlquery import sql_query, sql_queryM, sql_delete
    tarea = request.args.get('tareaS1')
    mens  = request.args.get('tareaS2')
    sql_delete(''' DELETE FROM TareaDetalles WHERE IDTarea = ? AND IDMensaje = ? ''', (tarea, mens) )
    results = sql_query(' SELECT * FROM TareaDetalles INNER JOIN Protocolos on Protocolos.ID = TareaDetalles.ProtocoloID WHERE "IDTarea" = "' + tarea + '";')
    selectN= sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (tarea,))
    return render_template('opciones-tareas.html', results=results, selectt = selectN[0])

@app.route('/opciones/tarea_r',methods = ['POST', 'GET'])               # Cambio de nombre de tarea
def sql_tarea_name():
    from app.sqlquery import sql_edit_insert, sql_query, sql_queryM
    if request.method == 'POST':
        IDTarea = request.form['IDTarea']
        new_name = request.form['name']
        sql_edit_insert(''' UPDATE Tareas set "Nombre"=? WHERE "IDTarea"=? ''', (new_name, IDTarea) )
    tareas = sql_query(''' SELECT * FROM Tareas''')
    selectN= sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (IDTarea,))
    return render_template('opciones-tareas.html', tareas=tareas, selectt = selectN[0])

@app.route('/opciones/tarea_rd',methods = ['POST', 'GET'])              # Cambio descripcion de tarea
def sql_tarea_desc():
    from app.sqlquery import sql_edit_insert, sql_query, sql_queryM
    if request.method == 'POST':
        IDTarea = request.form['IDTarea']
        desc = request.form['desc']
        sql_edit_insert(''' UPDATE Tareas set "Descripcion"=? WHERE "IDTarea"=? ''', (desc, IDTarea) )
    tareas = sql_query(''' SELECT * FROM Tareas''')
    selectN= sql_queryM(''' SELECT * FROM Tareas WHERE IDTarea=? ''', (IDTarea,))
    return render_template('opciones-tareas.html', tareas=tareas, selectt = selectN[0])

@app.route('/opciones/tarea_d',methods = ['POST', 'GET'])               # Eliminar tarea
def sql_tarea_delete():
    from app.sqlquery import sql_delete, sql_query, sql_queryM
    if request.method == 'GET':
        IDTarea = request.args.get('tareaS')
        try:
            sql_delete(''' DELETE FROM Tareas WHERE "IDTarea"=?''', (IDTarea,) )
        except Exception as e:
            print(e)
    tareas = sql_query(''' SELECT * FROM Tareas''')
    return render_template('opciones-tareas.html',tareas=tareas, selectt=["",""])


#####################
# Opciones - Proto  #
#####################

@app.route('/opciones/proto')                                           # Pagina principal de Protocolos
def sql_proto():
    from app.sqlquery import sql_query
    proto = sql_query(''' SELECT * FROM Protocolos''')
    return render_template('opciones-proto.html', proto = proto)

@app.route('/opciones/edit_proto')                                      # Lista de bloques de un protocolo
def sql_proto_query():
    from app.sqlquery import sql_query, sql_queryM
    protoS = request.args.get('protoS')
    protoN = request.args.get('protoN')
    results = sql_query(' SELECT * FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + protoS + '";')
    return render_template('opciones-proto.html', protoS=protoS, protoN=protoN , results=results)

@app.route('/opciones/edit_proto_n',methods = ['POST', 'GET'])          # Creacion de nuevo protocolo
def sql_proto_tcreate():
    from app.sqlquery import sql_edit_insert
    if request.method == 'POST':
        name = request.form['name']
        sql_edit_insert(''' INSERT into Protocolos(Nombre) VALUES(?) ''', (name,))
    return render_template('opciones-proto.html', select=name )

@app.route('/opciones/edit_proto_b',methods = ['POST', 'GET'])          # Creacion de nuevo bloque de protocolo
def sql_proto_bcreate():
    from app.sqlquery import sql_query, sql_edit_insert
    if request.method == 'POST':
        sql_edit_insert(''' INSERT into ProtocoloDetalles VALUES(?,?,?,?,?,?) ''', (str(request.form['proto']), str(request.form['Ord']),str(request.form['Nom']),str(request.form['TSelect']),str(request.form['Tam']),str(request.form['Def']),))
    results = sql_query(' SELECT * FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + request.form['proto'] + '";')
    return render_template('opciones-proto.html', protoS=request.form['proto'] , protoN=request.form['protoN'] , results=results)

@app.route('/opciones/edit_proto_e', methods = ['POST', 'GET'])         # Editar bloque de un protocolo
def sql_proto_bedit():
    from app.sqlquery import sql_query, sql_queryM, sql_edit_insert
    proto = request.form['proto']
    results = sql_query(' SELECT * FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '";')

    if str(request.form['Ord']) == str(request.form['Nom']) == str(request.form['Tam']) == str(request.form['Def']) == "":
        return render_template('opciones-tareas.html',results=results)
    if request.method == 'POST':
        sql_edit_insert(''' UPDATE ProtocoloDetalles set "EstrNombre"=?,"EstrTipo"=?,"EstrSize"=?,"EstrDefecto"=? WHERE "ID Protocolo" = ? AND "Orden" = ? ''', (str(request.form['Nom']), str(request.form['TSelect']), str(request.form['Tam']), str(request.form['Def']), str(request.form['proto']), str(request.form['Ord']),  ))
    return render_template('opciones-proto.html', protoS=request.form['proto'] , protoN=request.form['protoN'], results=results)

@app.route('/opciones/edit_proto_d')                                    # Eliminar bloque de un protocolo
def sql_proto_bdelete():
    from app.sqlquery import sql_delete, sql_query
    proto = request.args.get('proto')
    protoN = request.args.get('protoN')
    orden = request.args.get('orden')
    results = sql_query(' SELECT * FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '";')

    if request.method == 'GET':
        try:
            sql_delete(''' DELETE FROM ProtocoloDetalles WHERE "ID Protocolo" = ? AND "Orden" = ? ''', (proto, orden ,) )
        except Exception as e:
            print(e)
    results = sql_query(' SELECT * FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '";')
    return render_template('opciones-proto.html', protoS=proto , protoN=protoN, results = results)

@app.route('/opciones/evento_proto', methods = ['POST', 'GET'])                                    # Lista de eventos de un protocolo
def sql_evento_query():
    from app.sqlquery import sql_query, sql_queryM
    proto = request.args.get('protoS')
    protoName = request.args.get('protoN')
    results = sql_query(' SELECT * FROM Eventos WHERE "ID Protocolo" = "' + proto + '";')
    check = sql_query(' SELECT EstrSize FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '" AND EstrNombre = \"Codigo Evento\" ;')
    reglas = sql_query(' SELECT * FROM ReglasEstado ORDER BY "IDReglaEstado"')
    return render_template('opciones-proto.html', proto = proto, reglas = reglas, check = check, protoN=protoName , results=results)

@app.route('/opciones/evento_proto_n', methods = ['GET', 'POST'])                # Creacion de nuevo evento de protocolo
def sql_evento_create():
    from app.sqlquery import sql_edit_insert, sql_query
    if request.method == 'POST':
        proto = request.form['proto']
        protoName = request.form['protoN']
        sql_edit_insert(''' INSERT into Eventos VALUES(?,?,?,?,?) ''', (proto ,str(request.form['Cod']),str(request.form['Tip']),str(request.form['Reg']),str(request.form['Nom']),))
        results = sql_query(' SELECT * FROM Eventos WHERE "ID Protocolo" = "' + proto + '";')
        check = sql_query(' SELECT EstrSize FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '" AND EstrNombre = \"Codigo Evento\" ;')
        reglas = sql_query(' SELECT * FROM ReglasEstado ORDER BY "IDReglaEstado"')
        return render_template('opciones-proto.html', proto = proto, reglas = reglas, check = check, protoN=protoName , results=results)
    else:
        proto = sql_query(''' SELECT * FROM Protocolos''')
        return render_template('opciones-proto.html', proto = proto)


@app.route('/opciones/evento_proto_e', methods = ['POST', 'GET'])                                  # Editar evento de un protocolo
def sql_evento_edit():
    proto = request.form['proto']
    from app.sqlquery import sql_query, sql_queryM, sql_edit_insert
    if str(request.form['Cod']) == str(request.form['Tip']) == str(request.form['Reg']) == str(request.form['Nom']) == "":
        return render_template('opciones-tareas.html',results=results, selectt = selectN[0])
    sql_edit_insert(''' UPDATE Eventos set "Indice"=?,"TipoEstado"=?,"IDReglaEstado"=?, "Nombre"=? WHERE "ID Protocolo"=? AND "Indice"=? ''', (str(request.form['Cod']), str(request.form['Tip']), str(request.form['Reg']), str(request.form['Nom']), proto, str(request.form['oldEvento']) ) )
    results = sql_query(' SELECT * FROM Eventos WHERE "ID Protocolo" = "' + proto + '";')
    check = sql_query(' SELECT EstrSize FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '" AND EstrNombre = \"Codigo Evento\" ;')
    reglas = sql_query(' SELECT * FROM ReglasEstado ORDER BY "IDReglaEstado"')
    return render_template('opciones-proto.html', proto = proto, reglas = reglas, check = check, protoN="" , results=results)

@app.route('/opciones/evento_proto_d',methods = ['POST', 'GET'])         # Eliminar evento de un protocolo
def sql_evento_delete():
    print (request.args.get('protoS'), request.args.get('evento'))
    proto = request.args.get('protoS')
    evento = request.args.get('evento')
    from app.sqlquery import sql_query, sql_queryM
    if request.method == 'POST':
        try:
            sql_delete(''' DELETE FROM Eventos WHERE "ID Protocolo"=? AND "Indice"=?''', (proto, evento) )
        except Exception as e:
            print(e)

    results = sql_query(' SELECT * FROM Eventos WHERE "ID Protocolo" = "' + proto + '";')
    check = sql_query(' SELECT EstrSize FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '" AND EstrNombre = \"Codigo Evento\" ;')
    reglas = sql_query(' SELECT * FROM ReglasEstado ORDER BY "IDReglaEstado"')
    return render_template('opciones-proto.html', proto = proto, reglas = reglas, check = check, protoN="" , results=results)

@app.route('/opciones/proto_r',methods = ['POST', 'GET'])               # Cambio de nombre de un protocolo
def sql_proto_name():
    from app.sqlquery import sql_edit_insert, sql_query
    if request.method == 'POST':
        old_name = request.form['old_name']
        name = request.form['name']
        try:
            sql_edit_insert(''' UPDATE Tareas set "N. Tarea"=? WHERE "N. Tarea"=? ''', (name, old_name) )
        except Exception as e:
            print(e)
    proto = sql_query(''' SELECT DISTINCT "N. Tarea" FROM Tareas''')
    return render_template('opciones-proto.html', proto=proto)

@app.route('/opciones/proto_d',methods = ['POST', 'GET'])               # Eliminar protocolo
def sql_proto_delete():
    from app.sqlquery import sql_delete, sql_query
    if request.method == 'GET':
        protoS = request.args.get('protoS')
        try:
            sql_delete(''' DELETE FROM Protocolos WHERE "ID"=?''', (protoS,) )
        except Exception as e:
            print(e)
    proto = sql_query(''' SELECT * FROM Protocolos''')

    return render_template('opciones-proto.html',proto=proto)

@app.route('/opciones/evento_impo',methods = ['POST', 'GET'])               # Importar eventos
def cargar_eventos():
    from app.sqlquery import sql_delete, sql_edit_insert, sql_query
    file = request.files['file']
    proto = request.form['proto']
    protoN = request.form['protoN']
    if not file:
        return "No file"

    sql_delete(''' DELETE FROM Eventos WHERE "ID Protocolo"=? ''', (proto,) )
    file_contents = file.stream.read().decode("utf-8")
    reader = csv.reader(file_contents.split('\n'), delimiter=',')
    next(reader, None)
    for evento in reader:
        try:
            sql_edit_insert(''' INSERT into Eventos VALUES(?,?,?,?,?) ''', (proto, evento[1],evento[2], evento[3],evento[4],) )
        except IndexError:
            pass

    results = sql_query(' SELECT * FROM Eventos WHERE "ID Protocolo" = "' + proto + '";')
    check = sql_query(' SELECT EstrSize FROM ProtocoloDetalles WHERE "ID Protocolo" = "' + proto + '" AND EstrNombre = \"Codigo Evento\" ;')
    reglas = sql_query(' SELECT * FROM ReglasEstado ORDER BY "IDReglaEstado"')
    return render_template('opciones-proto.html', proto = proto, reglas = reglas, check = check, protoN=protoN , results=results)

@app.route('/opciones/evento_expo',methods = ['POST', 'GET'])               # Exportar eventos
def descargar_eventos():
    from app.sqlquery import sql_query
    eventos = sql_query(' SELECT * FROM Eventos WHERE "ID Protocolo" = "' + request.args.get('protoS') + '";')
    name = sql_query(' SELECT Nombre FROM Protocolos WHERE "ID" = "' + request.args.get('protoS') + '";')
    print (name)
    def generate(eventos):
        data = StringIO()
        archivo = csv.writer(data)

        archivo.writerow(('Codigo_Proto','Codigo_Evento', 'Tipo', 'Regla_Estado', 'Nombre'))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

        for item in eventos:
            archivo.writerow((
                item[0],
                item[1],
                item[2],
                item[3],
                item[4],
            ))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)
    headers = Headers()
    headers.set('Content-Disposition', 'attachment', filename=name[0][0] + '.csv')
    return Response(stream_with_context(generate(eventos)),mimetype='text/csv', headers=headers)


#####################
# Opciones - Reglas #
#####################

@app.route('/opciones/reglas')                                          # Pagina principal de reglas de estado
def sql_reglas():
    from app.sqlquery import sql_query
    reglas = sql_query(''' SELECT * FROM ReglasEstado''')
    return render_template('opciones-reglas.html',reglas=reglas)

@app.route('/opciones/edit_reglas_n',methods = ['POST', 'GET'])         # Creacion de nueva reglas de estado
def sql_reglas_tcreate():
    from app.sqlquery import sql_query, sql_edit_insert
    if request.method == 'POST':
        sql_edit_insert(''' INSERT into ReglasEstado VALUES(?,?) ''', (request.form['Idn'],request.form['Nombre']) )
    reglas = sql_query(''' SELECT * FROM ReglasEstado''')
    return render_template('opciones-reglas.html',reglas=reglas)

@app.route('/opciones/edit_reglas_e',methods = ['POST', 'GET'])         # editar reglas de estado
def sql_reglas_edit():
    from app.sqlquery import sql_edit_insert, sql_query
    if request.method == 'POST':
        old_name = request.form['old_name']
        Idn = request.form['Idn']
        Nombre = request.form['Nombre']
        try:
            sql_edit_insert(''' UPDATE ReglasEstado set "ID"=?, "NombreReglaEstado"=? WHERE "ID"=? ''', (Idn,Nombre, old_name) )
        except Exception as e:
            print(e)
    reglas = sql_query(''' SELECT * FROM ReglasEstado''')
    return render_template('opciones-reglas.html', reglas=reglas)

@app.route('/opciones/reglas_d',methods = ['POST', 'GET'])              # Eliminar reglas de estado
def sql_reglas_delete():
    from app.sqlquery import sql_delete, sql_query
    if request.method == 'GET':
        regla = request.args.get('reglaS')
        try:
            sql_delete(''' DELETE FROM ReglasEstado WHERE "ID"=?''', (regla,) )
        except Exception as e:
            print(e)
    reglas = sql_query(''' SELECT * FROM ReglasEstado''')
    return render_template('opciones-reglas.html',reglas=reglas)

@app.route('/opciones/reglas_impo',methods = ['POST', 'GET'])               # Importar reglas
def cargar_reglas():
    from app.sqlquery import sql_query, sql_delete, sql_edit_insert

    file = request.files['file']
    if not file:
        return "No file"

    sql_delete(''' DELETE FROM ReglasEstado ''', () )
    file_contents = file.stream.read().decode("utf-8")
    reader = csv.reader(file_contents.split('\n'), delimiter=',')
    next(reader, None)
    for regla in reader:
        try:
            sql_edit_insert(''' INSERT into ReglasEstado VALUES(?,?) ''', (regla[0],regla[1]) )
        except IndexError:
            pass


    reglas = sql_query(''' SELECT * FROM ReglasEstado''')
    return render_template('opciones-reglas.html',reglas=reglas)

@app.route('/opciones/reglas_expo',methods = ['POST', 'GET'])               # Exportar reglas
def descargar_reglas():
    from app.sqlquery import sql_query
    reglas = sql_query(' SELECT * FROM ReglasEstado ')
    def generate(reglas):
        data = StringIO()
        archivo = csv.writer(data)

        archivo.writerow(('Regla_estado', 'Nombre',))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

        for item in reglas:
            archivo.writerow((
                item[0],
                item[1]
            ))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)
    headers = Headers()
    headers.set('Content-Disposition', 'attachment', filename='export_reglas.csv')
    return Response(stream_with_context(generate(reglas)),mimetype='text/csv', headers=headers)



#####################
#       SocketIO    #
#####################


@socketio.on('SolicitarProtocolos', namespace='/test')                  # Solicitud simple de lista de protocolos
def solicitarProtocolos(message):
    from  app.Certificar import listarProtocolos 							
    msg = []
    emit('listaProtocolos', {'data': listarProtocolos()})

@socketio.on('SolicitarProtocolos2', namespace='/proto')                # Solicitud de eventos asociados a un protocolo, y extra info
def solicitarProtocolos2(message):
    from app.sqlquery import sql_query				
    msg = []
    msg.append(sql_query(' SELECT Indice, Nombre FROM Eventos WHERE "ID Protocolo" = ' + str(message["data"][0])))
    msg.append(sql_query(' SELECT EstrSize, EstrDefecto FROM ProtocoloDetalles WHERE "ID Protocolo" = ' + str(message["data"][0]) + ' AND EstrNombre = "Nro Abonado"')[0])
    msg.append(sql_query(' SELECT EstrSize, EstrDefecto FROM ProtocoloDetalles WHERE "ID Protocolo" = ' + str(message["data"][0]) + ' AND EstrNombre = "Nro Zona_User"')[0])
    msg.append([message["data"][0], message["data"][1], message["data"][2]])
    print (msg)
    emit('listaProtocolos2', {'data': msg})
    
@socketio.on('SolicitarProtocolos3', namespace='/test')                 # Solicitud simple de lista de protocolos
def solicitarProtocolos3():
    from app.sqlquery import sql_query 							
    msg = sql_query(''' SELECT * FROM Protocolos''')
    emit('listaProtocolos3', {'data': msg})

@socketio.on('SolicitarProtocolos4', namespace='/proto')                # Solicitud de eventos asociados a un protocolo
def solicitarProtocolos4(message):
    from app.sqlquery import sql_query
    msg = []
    msg.append(sql_query(' SELECT Indice, Nombre FROM Eventos WHERE "ID Protocolo" = "' + message["data"] + '"' ))
    msg.append(sql_query(' SELECT EstrSize, EstrDefecto FROM ProtocoloDetalles WHERE "ID Protocolo" = ' + message["data"] + ' AND EstrNombre = "Nro Abonado"')[0])
    msg.append(sql_query(' SELECT EstrSize, EstrDefecto FROM ProtocoloDetalles WHERE "ID Protocolo" = ' + message["data"] + ' AND EstrNombre = "Nro Zona_User"')[0])
    emit('listaProtocolos4', {'data': msg})

@socketio.on('SolicitarMensaje', namespace='/mesg')                     # Solicitar mensaje segun parametros                     
def solicitarMensaje(message):
    from app.sqlquery import sql_query
    from app.Certificar import generarMensajes
    protocolo = message["data"][0]
    if message["data"][1] != "NULL": evento = message["data"][1]
    else: evento = ""
    if message["data"][2] != "NULL": cliente = message["data"][2]
    else: cliente = ""
    if message["data"][3] != "NULL": zona = message["data"][3]
    else: zona = ""
    emit('mostrarMensaje', {'data': [generarMensajes(int(protocolo),str(evento),str(cliente),str(zona))[0], generarMensajes(int(protocolo),str(evento),str(cliente),str(zona))[1]] })

@socketio.on('enviarMensaje', namespace='/mesg')                        # Enviar mensaje
def enviarMensaje(message):
    from app.conexion import tcp_connect, tcp_close, tcpCli, envioMensaje
    global results
    if local: cli = threading.Thread(target=tcpCli, args=(message,)).start()
    s, conn, addr = tcp_connect()
    msg = []
    msg.append(message["data"][0])
    msg.append(message["data"][1])
    rel = envioMensaje(msg, check, local, conn)
    results.append(rel)
    emit('Resultados', {'data': rel})
    emit('Resultados2', {'data': rel})
    if not isinstance(conn, int): tcp_close(conn)

@socketio.on('ejecutarTarea', namespace='/mesg')                        # Ejecutar tarea
def ejecutarTarea(message):
    from app.sqlquery import sql_query
    from app.Certificar import generarMensajes
    from app.conexion import tcp_connect, tcp_close, tcpCli, envioMensaje
    global results
    msg = []
    tarea = sql_query(' SELECT * FROM TareaDetalles WHERE "IDTarea" = "' + message['data'] + '"')
    if local: cli = threading.Thread(target=tcpCli, args=(tarea,)).start()
    time.sleep(0.1)
    s, conn, addr = tcp_connect()
    for m in tarea:
        rel = envioMensaje(generarMensajes(int(m[2]),str(m[3]),str(m[4]),str(m[5])), check, local, conn)
        results.append(rel)
        emit('Resultados', {'data': rel})
        time.sleep(0.01)
    emit('Resultados2', {'data': rel})
    if not isinstance(conn, int): tcp_close(conn)
        

@socketio.on('ejecutarProtocolo', namespace='/mesg')                    # Probar protocolo entero       
def ejecutarProto(message):
    from app.sqlquery import sql_query
    from app.Certificar import generarMensajes, Prueba
    from app.conexion import tcp_connect, tcp_close, tcpCli, envioMensaje
    msg = []
    global results
    eventos = sql_query(' SELECT Indice FROM Eventos WHERE "ID Protocolo" = "' + message['data'] + '"')
    if local: cli = threading.Thread(target=tcpCli, args=(eventos,)).start()
    time.sleep(0.1)
    s, conn, addr = tcp_connect()
    for m in eventos:
        rel = envioMensaje(generarMensajes(int(message['data']),m[0],"",""), check, local, conn)
        results.append(rel)
        emit('Resultados', {'data': rel})
        time.sleep(0.2)
    emit('Resultados2', {'data': rel})
    if not isinstance(conn, int): tcp_close(conn)

@socketio.on('SolicitarTarea', namespace='/mesg')                       # Solicitar lista de tareas           
def solicitarTarea(message):
    from app.sqlquery import sql_query
    tarea = sql_query('SELECT * FROM Tareas WHERE "N. Tarea" = \'' + message["data"][0] + '\' AND "N. Mensaje" = \'' + message["data"][1] + '\'')
    emit('mensajeInfo', {'data': tarea})

@socketio.on('Checkbox', namespace='/check')                            # Opciones de sistema             
def checkbox(message):
    global check
    global local
    if message['data'][0] == 0:
        check = message['data'][1]
    if message['data'][0] == 1:
        local = message['data'][1]

@socketio.on('info', namespace='/check')                                # Parametros de conexion
def getinfo(message):
    if message['data'][0] is not "":
        config['OPCIONES']['NEVULA_TCP_IP'] = message['data'][0]
    if message['data'][1] is not "":
        config['OPCIONES']['NEVULA_TCP_PORT'] = message['data'][1]
    if message['data'][2] is not "":
        config['OPCIONES']['LOG_SSH_IP'] = message['data'][2]
    if message['data'][3] is not "":
        config['OPCIONES']['LOG_SSH_PORT'] = message['data'][3]
    if message['data'][4] is not "":
        config['OPCIONES']['TIMEOUT_TCP'] = message['data'][4]
    if message['data'][5] is not "":
        config['OPCIONES']['TIMEOUT_MSG'] = message['data'][5]

    with open('config.ini', 'w') as configfile:
        config.write(configfile)

if __name__ == "__main__":
    #app.run(debug=True)
    socketio.run(app, debug=True)

