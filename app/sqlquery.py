import os, sqlite3
import pandas as pd

#Conexion a base de  datos
conn = sqlite3.connect(os.path.join('app', 'BaseDatos.db'), check_same_thread=False)

def sql_query(query):   #Ejecutar query
    cur = conn.cursor()
    cur.execute(query)
    rows = cur.fetchall()
    return rows

def sql_queryM(query,var):  #Ejecutar query con parametros a parte
    cur = conn.cursor()
    cur.execute(query,var)
    rows = cur.fetchall()
    return rows

def sql_edit_insert(query,var): #Ejecutar query para insertar o editar
    try:
        cur = conn.cursor()
        cur.execute(query,var)
        conn.commit()
        return 0
    except sqlite3.OperationalError as e:
        conn.rollback()
        return 1
    except Exception as e:
        conn.rollback()
        raise e
    finally:
        cur.close()

def sql_delete(query,var):  #Ejecutar query para eliminar
    try:
        cur = conn.cursor()
        cur.execute(query,var)
        conn.commit()
    except Exception as e:
        conn.rollback()
        raise e
    finally:
        cur.close()


