import socket, threading, string, time, os, paramiko, subprocess, sys, configparser

from app.reporte import certificar

config = configparser.ConfigParser()
config.read('config.ini')


def leerLinea(): # Leer linea de archivo en SSH
        if os.name != 'nt':
                return subprocess.Popen(['tail', '-1', config.LOG_FILE], stdout=subprocess.PIPE).stdout.read()
        else:
                return ""

def ssh_connect():
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        SSH_ip          = config['OPCIONES']['LOG_SSH_IP']
        SSH_port        = int(config['OPCIONES']['LOG_SSH_PORT'])
        SSH_user        = config['OPCIONES']['LOG_USER']
        SSH_pass        = config['OPCIONES']['LOG_PASS']
        try:
                ssh.connect(SSH_ip, port=SSH_port, username=SSH_user, password=SSH_pass)
                sftp = ssh.open_sftp()
                sftp.chdir("/tmp/")
        except Exception as e:
                return 1
        try:                        
                print(sftp.stat(config['OPCIONES']['LOG_FILE']))
                stdin, stdout, stderr = ssh.exec_command('ls -l')
                print('Log Encontrado')
        except IOError:
                return 2
        return ssh

def tcpCli(MESSAGE): # Receptora de mensajes para pruebas
        try:
                time.sleep(1)
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(int(config['OPCIONES']['TIMEOUT_TCP']))
                s.connect(("127.0.0.1", int(config['OPCIONES']['NEVULA_TCP_PORT'])))
                s.sendto(bytes("A", "utf-8"), ("127.0.0.1", int(config['OPCIONES']['NEVULA_TCP_PORT'])))
        except socket.error as e:
                print ("Error de cliente " + str(e))
                s.close()
                exit()
        else:
                for m in MESSAGE:
                        msg = s.recv(int(config['OPCIONES']['BUFFER_SIZE']))
                        print("-"*120)
                        print("Mensaje Recibido: " + str(msg))
                        print("-"*120)
        s.close()

def tcp_connect():
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.settimeout(int(config['OPCIONES']['TIMEOUT_TCP']))
        #ack = bytes(chr(0x06), "utf-8")
        s.bind(("0.0.0.0", int(config['OPCIONES']['NEVULA_TCP_PORT'])))
        s.listen(0)
        try:
                conn, addr = s.accept()
                print ("Conectado")
                return s, conn, addr
        except Exception as e:
                return 1, 1, 1

def tcp_connect_2():
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(int(config['OPCIONES']['TIMEOUT_TCP']))
        s.connect((config['OPCIONES']['NEVULA_TCP_IP'], int(config['OPCIONES']['NEVULA_TCP_PORT'])))
        s.sendto(bytes("A", "utf-8"), (config['OPCIONES']['NEVULA_TCP_IP'], int(config['OPCIONES']['NEVULA_TCP_PORT'])))
        msg = s.recv(int(config['OPCIONES']['BUFFER_SIZE']))

def tcp_close(socket):
        socket.close()


def envioMensaje(MESSAGE, check, local, conn): #Envio de una lista de mensajes
        resultado = []

        if MESSAGE is 1:
                resultado = []
                print("Enviado")
                resultado.append("Error")
                resultado.append(-1)
                resultado.append("Mensaje no Generado")
                resultado.append("Error al generar mensaje")
                resultados.append(resultado)

        if isinstance(conn, int):
                print("No Enviado, Servidor no encontrado")
                resultado.append("Error")
                resultado.append(-1)
                resultado.append("Mensaje no enviado, Servidor no encontrado")
                resultado.append("")
                return resultado
        else:
                if not check:
                        ssh = ssh_connect()
                resultado = []
                timeout = 0
                last_line = ''
                
                if not check and not isinstance(ssh, int):
                        try:
                                stdin, stdout, stderr = ssh.exec_command('tail -1 ' + config['OPCIONES']['LOG_FILE'])
                                last_line = stdout.read()
                        except paramiko.SSHException:
                                print("Error de Conexión SSH")
                                print("-"*120)
                        else:
                                
                                conn.send((bytes(MESSAGE[0], "utf-8")) )
                                cur_line = last_line
                                while ((cur_line == last_line) and (timeout < int(config['OPCIONES']['TIMEOUT_MSG']))):
                                        try:
                                                stdin, stdout, stderr = ssh.exec_command('tail -1 ' + config['OPCIONES']['LOG_FILE'])
                                                cur_line = stdout.read()
                                        except paramiko.SSHException:
                                                print("Error de Conexión SSH")
                                                print("-"*120)
                                        print (cur_line," + ", timeout, end="\r")
                                        time.sleep(1)
                                        timeout += 1
                                mod = cur_line
                                print(mod)
                else:
                        conn.send((bytes(MESSAGE[0], "utf-8")) )
                        mod = ""

                print (" ")
                print("-"*120)
                print("Enviando mensaje")
                print("-"*120)
                print (MESSAGE[0])
                print("-"*120)

                if check:
                        print("Enviado")
                        resultado.append(MESSAGE[0])
                        resultado.append(3)
                        resultado.append("Enviado")
                        resultado.append("Mensaje enviado sin verificar")
                else:
                        case = certificar(MESSAGE, mod)
                        caseSelect = int(case[0])
                        if caseSelect == 0:
                                print("Correcto")
                                resultado.append(MESSAGE[0])
                                resultado.append(0)
                                resultado.append("Correcto")
                                resultado.append(case[1])
                        elif caseSelect == 1:
                                print("Incorrecto")
                                resultado.append(MESSAGE[0])
                                resultado.append(1)
                                resultado.append("Incorrecto")
                                resultado.append("Regla de estado no coincide. " + case[1])
                        elif caseSelect == 2:
                                print("Rechazado")
                                resultado.append(MESSAGE[0])
                                resultado.append(2)
                                resultado.append("Rechazado")
                                resultado.append("Mensaje fue rechazado por el receptor")
                        elif caseSelect == 3:
                                print("No verificado")
                                resultado.append(MESSAGE[0])
                                resultado.append(3)
                                resultado.append("No verificado")
                                resultado.append("Mensaje enviado, no verificado")
                        else:
                                print("Error")
                                resultado.append(MESSAGE[0])
                                resultado.append(-1)
                                resultado.append("Error")
                                resultado.append("Error desconocido")

                #ssh.close()
                #print ('Recibiendo Ack')               // Normalmente una receptora recibe un mensaje de reconocimiento (ACK),
                #data = conn.recv(config.BUFFER_SIZE)   // Y la receptora lo retransmite en caso de recibir un NACK, o envia un ACK si no recibe ninguno
                #print ('Enviando Ack')                 // Sin embargo, Nevula tiene problemas en este proceso por tcp           
                #conn.send(ACK)                         // Por lo tanto se encuentra deshabilitado
                return resultado