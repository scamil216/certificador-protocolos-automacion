import importlib, json, os
import os, sqlite3
from app.sqlquery import sql_query
from pandas.io.json import json_normalize
from array import *

def generarMensaje(protocolo, evento = "", cliente = "", zona = "", regla = ""):

    if isinstance(evento, (list,)):
        evento = evento[0]

    protoEvento = ""
    if isinstance(protocolo, int):
        try:
            protoEstr = sql_query(' SELECT EstrNombre, EstrTipo, EstrSize, EstrDefecto FROM ProtocoloDetalles WHERE "ID Protocolo" = ' + str(protocolo) + ' ORDER BY Orden')
            proto = protocolo
            if not protoEstr: raise Exception('Protocolo no encontrado')
        except Exception as e:
            print ("Error al generar mensaje: Protocolo no existe")
            return 1
    else:  
        try:
            proto = sql_query('SELECT ID FROM Protocolos WHERE Nombre = \'' + protocolo + '\'')
            if not proto: 
                raise Exception('Protocolo no encontrado')
            protoEstr = sql_query(' SELECT EstrNombre, EstrTipo, EstrSize, EstrDefecto FROM ProtocoloDetalles WHERE "ID Protocolo" = ' + str(proto[0][0]) + ' ORDER BY Orden')
            if not protoEstr: 
                raise Exception('Protocolo no encontrado')
        except Exception as e:
            print ("Error al generar mensaje: Protocolo no existe")
            return 1
    sublist = []
    mesg = []
    for p in protoEstr:
        if p[1] == 'ascii':
            mesg.append(chr(int(p[3])))
        elif p[0] == 'Codigo Evento' and evento == "" and protoEvento == "":
            mesg.append(p[3])
            evento = p[3]
        elif p[0] == 'Codigo Evento' and evento != "" and len(evento) == p[2]:
            mesg.append(evento)
        elif p[0] == 'Nro Zona_User' and zona != "":
            if (len(zona) == p[2]):
                mesg.append(zona)
            elif (len(zona) < p[2]):
                mesg.append(zona.zfill(p[2]))
            else:
                return 1
        elif p[0] == 'Nro Abonado' and cliente != "":
            if (len(cliente) == p[2]):
                mesg.append(cliente)
            elif (len(cliente) < p[2]):
                mesg.append(cliente.zfill(p[2]))
            else:
                return 1
        else:
            mesg.append(p[3])
    sublist.append(''.join(str(x) for x in mesg))
    protoEvento = sql_query(' SELECT IDReglaEstado FROM Eventos WHERE "ID Protocolo" = ' + str(proto) + ' AND "Indice" = "' + evento + '"')
    if protoEvento:
        sublist.append(protoEvento[0][0])
    else:
        sublist.append("Sin Codigo")
    return sublist
    

