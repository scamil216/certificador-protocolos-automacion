import threading,io,sys,os,time,argparse,sqlite3, argparse
from time import sleep
from app.generarMesj import generarMensaje
from app.conexion import tcp_connect, tcp_close, tcpCli, envioMensaje
import app.conexion


def listarProtocolos():
	pro = []
	conn = sqlite3.connect('app/BaseDatos.db')
	c = conn.cursor()
	c.execute('SELECT Nombre FROM Protocolos')
	pro = c.fetchall()
	return sum(pro, ())

def generarMensajes(protocolo, evento, cliente = "", zona = ""):
	return (generarMensaje(protocolo, evento, cliente, zona))

def Prueba(mensaje, check, local, conn):
	if not local:
		return app.conexion.envioMensaje(v, check, local, conn)
	else:
		from multiprocessing.pool import ThreadPool
		pool = ThreadPool(processes=1)
		cli = threading.Thread(target=app.conexion.tcpCli, args=(mensaje,)).start()
		sleep(0.1)
		sev = pool.apply_async(app.conexion.envioMensaje, args=(mensaje,check,local, conn))
		cli.join()
		return sev.get()
