import os, time, json
from app.sqlquery import sql_query
import paramiko

def certificar(mensaje, mod):
    resp = []
    if mod == "":
        resp.append(3)
        return resp
    #lines = mod.splitlines()
    #SRule = lines[-1].split( )[2]
    
    SRule = mod.split( )[2]
    if ":" in SRule.decode():
        try:
            SRule = SRule.decode().split(":")[1]
        except exeption as e:
            resp.append(4)
    else:
        resp.append(2)
        return resp
    SRuleText = sql_query('SELECT * FROM ReglasEstado WHERE IDReglaEstado = "' + SRule + '"')[0][1] + "(" + SRule + ')'
    SRuleEsperado = mensaje[1]
    SRuleEsperadoText = sql_query('SELECT * FROM ReglasEstado WHERE IDReglaEstado = "' + SRuleEsperado + '"')[0][1] + "(" + SRuleEsperado + ')'
    
    if SRule == mensaje[1]:
            resp.append(0)
            resp.append("Estado esperado:(" + SRuleEsperadoText + ") <br>Estado Recibido:" + SRuleText + ")")
    else:
        resp.append(1)
        resp.append("Estado esperado:(" + SRuleEsperadoText + ") <br>Estado Recibido:" + SRuleText + ")")
    print("Estado esperado:(",SRuleEsperadoText,")\nEstado Recibido:",SRuleText,")")
    return resp